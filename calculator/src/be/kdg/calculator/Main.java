package be.kdg.calculator;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import be.kdg.calculator.model.Calculator;
import be.kdg.calculator.view.CalculatorPane;

public class Main extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Calculator model = new Calculator();
        CalculatorPane view = new CalculatorPane();



        primaryStage.setTitle("Calculator");
        primaryStage.setScene(new Scene(view));
        // TODO: Uncomment the following line as soon as you've implemented the constructor.
        //Presenter presenter = new Presenter(model, view);
        primaryStage.show();
    }
}
