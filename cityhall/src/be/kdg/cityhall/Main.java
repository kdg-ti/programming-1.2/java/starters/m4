package be.kdg.cityhall;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import be.kdg.cityhall.view.Presenter;
import be.kdg.cityhall.view.CityHallPane;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        final CityHallPane view = new CityHallPane();
        primaryStage.setTitle("City Hall");
        primaryStage.setScene(new Scene(view));
        final Presenter presenter = new Presenter(view);
        primaryStage.show();
    }
}
